<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\IndexGroupController;
use App\Http\Controllers\TechnicalEvaluationController;
use App\Http\Controllers\TechnicalEvaluationIndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TechnicalEvaluationController::class, 'index'])->name('index');

Route::middleware('guest')->group(function () {
    Route::get('/register', [UserController::class, 'registerView'])->name('register');
    Route::post('/user/register', [UserController::class, 'register']);

    Route::get('/login', [UserController::class, 'loginView'])->name('login');
    Route::post('/user/login', [UserController::class, 'login']);
});

Route::middleware('auth')->group(function () {
    Route::get('/user/logout', [UserController::class, 'logout'])->name('logout');

    Route::get('/groups/create', [IndexGroupController::class, 'create'])->name('create.group');
    Route::post('/groups/store', [IndexGroupController::class, 'store']);
    Route::get('/groups/{id}/update', [IndexGroupController::class, 'edit'])->name('edit.group');
    Route::put('/groups/{id}/update', [IndexGroupController::class, 'update']);
    Route::get('/groups/api', [IndexGroupController::class, 'fakeAPI']);

    Route::get('/hotels/create', [HotelController::class, 'create'])->name('create.hotel');
    Route::post('/hotels/store', [HotelController::class, 'store']);
    Route::get('/hotels/{id}/update', [HotelController::class, 'edit'])->name('edit.hotel');
    Route::put('/hotels/{id}/update', [HotelController::class, 'update']);
    Route::get('/hotels/api', [HotelController::class, 'fakeAPI']);
    Route::get('/hotels/{id}', [HotelController::class, 'show'])->name('show.hotel');

    Route::get('/evaluations/create', [TechnicalEvaluationController::class, 'create'])->name('create.evaluation');
    Route::post('/evaluations/store', [TechnicalEvaluationController::class, 'store']);
    Route::patch('/evaluations/{id}/updateName', [TechnicalEvaluationController::class, 'updateName']);
    Route::patch('/evaluations/{id}/updateHotel', [TechnicalEvaluationController::class, 'updateHotelId']);
    Route::delete('/evaluations/{id}/inative', [TechnicalEvaluationController::class, 'delete']);
    Route::post('/evaluations/{id}/active', [TechnicalEvaluationController::class, 'active']);
    Route::get('/evaluations/api', [TechnicalEvaluationController::class, 'fakeAPI']);
    Route::get('/evaluations/inatives/api', [TechnicalEvaluationController::class, 'fakeInativesAPI']);
    Route::get('/evaluations/{id}', [TechnicalEvaluationController::class, 'show'])->name('show.aval');

    Route::get('/indexes/create', [TechnicalEvaluationIndexController::class, 'create'])->name('create.index');
    Route::post('/indexes/store', [TechnicalEvaluationIndexController::class, 'store']);
    Route::get('/indexes/{id}/update', [TechnicalEvaluationIndexController::class, 'edit'])->name('edit.index');
    Route::put('/indexes/{id}/update', [TechnicalEvaluationIndexController::class, 'update']);
    Route::get('/indexes/api', [TechnicalEvaluationIndexController::class, 'fakeAPI']);
});

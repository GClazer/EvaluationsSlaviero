@extends('layouts.main')
@section('title', 'Index')

@section('content')

    @auth
        <a href="{{ route('logout') }}">Logout</a>
    @endauth

    <form-hotel></form-hotel>

    @if (session('response'))
        {{ session('response') }}
    @endif
@endsection
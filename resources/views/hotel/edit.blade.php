@extends('layouts.main')
@section('title', $hotel->name . ' - ' . $hotel->acronym)

@section('content')


    @auth
        <a href="{{ route('logout') }}">Logout</a>
    @endauth

    <form-hotel
    :id_hotel="{{ $hotel->id }}"
    name_hotel="{{ $hotel->name }}"
    acronym_hotel="{{ $hotel->acronym }}"
    method_form="put"
    ></form-hotel>

    @if (session('response'))
        {{ session('response') }}
    @endif
@endsection
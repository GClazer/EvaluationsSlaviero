@extends('layouts.main')
@section('title', 'Index')

@section('content')

    @auth
        <a href="{{ route('logout') }}">Logout</a>
    @endauth
    
    <form-index-group
    :id_group={{ $indexGroup->id }}
    description_group="{{ $indexGroup->description }}"
    :technicalindex_id="{{ $indexGroup->evaluation->id }}"
    method_form="put"></form-index-group>

    @if (session('response'))
        {{ session('response') }}
    @endif
@endsection
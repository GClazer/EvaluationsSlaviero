@extends('layouts.main')
@section('title', 'Index')

@section('content')

    @auth
        <a href="{{ route('logout') }}">Logout</a>
    @endauth

    <form-index-group></form-index-group>

    @if (session('response'))
        {{ session('response') }}
    @endif
@endsection
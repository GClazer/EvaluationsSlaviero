@extends('layouts.main')
@section('title', 'Index')

@section('content')

    @auth
        <a href="{{ route('logout') }}">Logout</a>
    @endauth

    <form-technical-evaluation-index
    :id_technicalindex="{{ $evaluation->id }}"
    name_technicalindex="{{ $evaluation->name }}"
    :id_evaluation="{{ $evaluation->evaluation->id }}"
    method_form="put"></form-technical-evaluation-index>

    @if (session('response'))
        {{ session('response') }}
    @endif
@endsection
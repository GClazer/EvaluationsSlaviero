@extends('layouts.main')
@section('title', 'Index')

@section('content')

    @auth
        <a href="{{ route('logout') }}">Logout</a>
    @endauth

    <form-technical-evaluation-index></form-technical-evaluation-index>

    @if (session('response'))
        {{ session('response') }}
    @endif
@endsection
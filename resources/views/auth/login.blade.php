@extends('layouts.main')
@section('title', 'Login')

@section('content')

    @if (session('response'))
        <h3 class="text-center">{{ session('response') }}</h3>
    @endif

    <form-login></form-login>
    
@endsection
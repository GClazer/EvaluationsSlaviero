@extends('layouts.main')
@section('title', 'Register')

@section('content')

    @if (session('response'))
        <h3 class="text-center">{{ session('response') }}</h3>
    @endif

    <form-register></form-register>
    
@endsection
@extends('layouts.main')
@section('title', 'Index')

@section('content')

    @auth
        <a href="{{ route('logout') }}">Logout</a>
    @endauth

    <form-technical-evaluation></form-technical-evaluation>

    @if (session('response'))
        {{ session('response') }}
    @endif
@endsection
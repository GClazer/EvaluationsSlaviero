@extends('layouts.main')
@section('title', $evaluation->name . ' - ' . $evaluation->hotel->acronym)

@section('content')


    {{ $evaluation }}

    @if (session('response'))
        {{ session('response') }}
    @endif
@endsection
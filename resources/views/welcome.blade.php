@extends('layouts.main')

@section('title', 'Index')

@section('content')
    @auth
    <h2>Olá {{ $user->name }}</h2>

    <a href="{{ route('logout') }}">Logout</a>
    <br>
    <a href="{{ route('create.hotel') }}">Criar hotel</a>
    <br>
    <a href="{{ route('create.evaluation') }}">Criar avaliação</a>
    <br>
    <a href="{{ route('create.index') }}">Criar índice</a>
    <br>
    <a href="{{ route('create.group') }}">Criar grupo</a>

    @endauth

    @guest
    <h2><a href="{{ route('login') }}">Login</a></h2>
    <br>
    <h2><a href="{{ route('register') }}">Register</a></h2>
    @endguest

    <table-evaluations></table-evaluations>

    @if (session('response'))
    {{ session('response') }}
    @endif
@endsection
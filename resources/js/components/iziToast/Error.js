export function errorResponse (title, message) {
    iziToast.show({
        title: title,
        message: message,
        color: 'red',
        position: 'topRight',
        timeout: 3000,
    });
}
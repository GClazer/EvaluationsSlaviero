export function successResponse (title, message) {
    iziToast.show({
        title: title,
        message: message,
        color: 'green',
        position: 'topRight',
        timeout: 3000,
    });
}
import './bootstrap';

import Vue from 'vue';

import FormRegister from './components/auth/Register.vue';
import FormLogin from './components/auth/Login.vue';
import IndexGroupForm from './components/main_forms/IndexGroupForm.vue';
import TechnicalEvaluationsForm from './components/main_forms/TechnicalEvaluationsForm.vue';
import TechnicalEvaluationIndexForm from './components/main_forms/TechnicalEvaluationIndexForm.vue';
import HotelForm from './components/main_forms/HotelForm.vue';
import InputSearch from './components/inputs/InputSearch.vue';
import EvaluationsTable from './components/tables/EvaluationsTable.vue';
import ModalRegisterEvaluations from './components/modals/RegisterEvaluationModal.vue';

// AUTH
Vue.component("form-register", FormRegister);
Vue.component("form-login", FormLogin);

// MAIN FORMS
Vue.component("form-technical-evaluation", TechnicalEvaluationsForm);
Vue.component("form-index-group", IndexGroupForm);
Vue.component("form-technical-evaluation-index", TechnicalEvaluationIndexForm);
Vue.component("form-hotel", HotelForm);

// INPUTS
Vue.component("input-search", InputSearch);

// TABLES
Vue.component("table-evaluations", EvaluationsTable);

// MODALS
Vue.component("modal-register-evaluation", ModalRegisterEvaluations)

new Vue({}).$mount("#app");
<?php

namespace App\Http\Controllers;

use App\Models\TechnicalEvaluation;
use Illuminate\Http\Request;

class TechnicalEvaluationController extends Controller
{
    public function index ()
    {
        $user = auth()->user();
        $evaluations = TechnicalEvaluation::get();

        return view('welcome', [
            'user' => $user,
            'evaluations' => $evaluations,
        ]);
    }

    public function create () 
    {
        return view('technical_evaluation.create');
    }

    public function store (Request $request) 
    {
        TechnicalEvaluation::create([
            'name' => $request->name,
            'hotel_id' => $request->hotel_id,
        ]);

        return session()->flash("response", "Avaliação criada com sucesso !!");
    }

    public function show ($id) 
    {
        $evaluation = TechnicalEvaluation::findOrFail($id);

        return view('technical_evaluation.show', [
            'evaluation' => $evaluation->load('indexes.groups'),
        ]);
    }

    public function updateName (Request $request, $id) 
    {
        TechnicalEvaluation::findOrFail($id)->update([
            'name' => $request->name,
        ]);
    }

    public function updateHotelId (Request $request, $id) 
    {
        TechnicalEvaluation::findOrFail($id)->update([
            'hotel_id' => $request->hotel_id,
        ]);
    }
    
    public function delete (Request $request, $id) 
    {
        TechnicalEvaluation::findOrFail($id)->delete();
    }

    public function active (Request $request, $id) 
    {
        TechnicalEvaluation::withTrashed()
            ->findOrFail($id)
            ->restore();
    }

    public function fakeAPI () 
    {
        $evaluations = TechnicalEvaluation::query()
            ->with('hotel')
            ->orderByDesc('created_at')
            ->get();

        return json_encode($evaluations);
    }

    public function fakeInativesAPI () 
    {
        $evaluations = TechnicalEvaluation::withTrashed()
            ->with('hotel')
            ->orderByDesc('deleted_at')
            ->where('deleted_at', '!=', null)
            ->get();

        return json_encode($evaluations);
    }

}

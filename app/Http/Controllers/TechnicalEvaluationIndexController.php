<?php

namespace App\Http\Controllers;

use App\Models\TechnicalEvaluationIndex;
use Illuminate\Http\Request;

class TechnicalEvaluationIndexController extends Controller
{
    public function create () 
    {
        return view('technical_evaluation_index.create');
    }

    public function store (Request $request) 
    {
        $name = $request->name;
        $technical_evaluation_id = $request->technical_evaluation_id;

        TechnicalEvaluationIndex::create([
            'name' => $name,
            'technical_evaluation_id' => $technical_evaluation_id,
        ]);

        return session()->flash("response", 'Índice criado com sucesso !!');
    }

    public function edit ($id) 
    {
        $evaluation = TechnicalEvaluationIndex::findOrFail($id)->load('evaluation');

        return view('technical_evaluation_index.edit', [
            'evaluation' => $evaluation,
        ]);
    }

    public function update (Request $request, $id) 
    {
        TechnicalEvaluationIndex::findOrFail($id)->update($request->all());
    }

    public function fakeAPI () 
    {
        $indexes = TechnicalEvaluationIndex::query()
            ->with('evaluation.hotel')
            ->get();

        return json_encode($indexes);
    }
}

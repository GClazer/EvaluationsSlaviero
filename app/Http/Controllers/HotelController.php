<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    public function create () 
    {
        return view('hotel.create');
    }

    public function store (Request $request) 
    {
        Hotel::create([
            'name' => $request->name,
            'acronym' => $request->acronym,
        ]);

        return session()->flash("response", "Hotel criado com sucesso !!"); 
    }

    public function show ($id) 
    {
        return;
    }

    public function edit (int $id) 
    {
        $hotel = Hotel::findOrFail($id);

        return view('hotel.edit', [
            'hotel' => $hotel,
        ]);
    }

    public function update (Request $request, $id) 
    {
        Hotel::findOrFail($id)->update($request->all());
    }

    public function fakeAPI () 
    {
        $hotels = Hotel::get();

        return json_encode($hotels);
    }
}

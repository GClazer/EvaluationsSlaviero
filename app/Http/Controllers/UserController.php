<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function registerView () 
    {
        return view('auth.register');
    }

    public function register (Request $request) 
    {
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;

        User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        return session()->flash("response", "Usuário criado com sucesso");
    }

    public function loginView () 
    {
        return view('auth.login');
    }

    public function login (Request $request) 
    {
        $email = $request->email;
        $password = $request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) 
        {
            return session()->flash("response", "Bem-vindo ao sistema !!");
        }

        return session()->flash("response", "Email e/ou senha inválidos");
    }

    public function logout () 
    {
        Auth::logout();

        return redirect('/')->with("response", "Usuário deslogado com sucesso !");
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\IndexGroup;
use App\Models\IndexGroupImage;
use Illuminate\Http\Request;

class IndexGroupController extends Controller
{
    public function create()
    {
        return view('index_group.create');
    }

    public function store(Request $request)
    {
        $description = $request->description;

        $group = IndexGroup::create([
            'description' => $description,
            'technical_evaluation_index_id' => $request->index_id,
        ]);

        if ($request->images > 0) {
            foreach ($request->images as $key => $value) {
                if ($value && $value->isValid()) {
                    $file = $value;
                    $extension = $file->extension();
                    $name_file = $file->getClientOriginalName();
                    $new_name = md5($name_file  . strtotime('now')) . "." . $extension;
                    $file->move(public_path('src/images/groups'), $new_name);
    
                    IndexGroupImage::create([
                        'path' => $new_name,
                        'index_group_id' => $group->id,
                    ]);
                }
            }
        }



        return session()->flash("response", "Grupo criado com sucesso !!");
    }

    public function edit ($id) 
    {
        $indexGroup = IndexGroup::findOrFail($id);

        return view('index_group.edit', [
            'indexGroup' => $indexGroup,
        ]);
    }

    public function update (Request $request, $id) 
    {
        $group = IndexGroup::findOrFail($id)->update([
            'description' => $request->description,
            'technical_evaluation_index_id' => $request->index_id,
        ]);
    }

    public function fakeAPI()
    {
        $groups = IndexGroup::query()
            ->with('images')
            ->with('evaluation')
            ->get();

        return json_encode($groups);
    }
}

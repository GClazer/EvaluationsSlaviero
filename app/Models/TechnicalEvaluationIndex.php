<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechnicalEvaluationIndex extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'technical_evaluation_id',
    ];

    public function evaluation () 
    {
        return $this->belongsTo(TechnicalEvaluation::class, 'technical_evaluation_id');
    }

    public function groups () 
    {
        return $this->hasMany(IndexGroup::class);
    }
}

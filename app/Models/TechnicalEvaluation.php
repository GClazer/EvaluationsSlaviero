<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechnicalEvaluation extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'hotel_id',
    ];

    public function indexes () 
    {
        return $this->hasMany(TechnicalEvaluationIndex::class);
    }

    public function hotel () 
    {
        return $this->belongsTo(Hotel::class, 'hotel_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndexGroupImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'path',
        'index_group_id',
    ];

    public function group () 
    {
        return $this->belongsTo(GrupoIndice::class, 'index_group_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IndexGroup extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'description',
        'technical_evaluation_index_id',
    ];

    public function evaluation()
    {
        return $this->belongsTo(TechnicalEvaluationIndex::class, 'technical_evaluation_indice_id');
    }

    public function images()
    {
        return $this->hasMany(IndexGroupImage::class);
    }
}
